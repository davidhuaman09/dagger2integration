package com.example.android.dagger.di

import android.content.Context
import com.example.android.dagger.main.MainActivity
import com.example.android.dagger.registration.RegistrationActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/*
* Una @Component interfaz proporciona la información que Dagger
* necesita para generar el gráfico en tiempo de compilación.
* El parámetro de los métodos de interfaz define qué clases
* solicitan la inyección.
 */

//Definición de un componente Dagger que agrega información del StorageModule al gráfico
@Singleton
@Component(modules = [StorageModule::class])
interface AppComponent {

    //Factory para crear instancias del AppComponent
    @Component.Factory
    interface Factory {
        /*
        *@BindsInstance le dice a Dagger que necesita agregar esa instancia en el
        * gráfico y siempre que Context sea ​​necesario, proporcione esa instancia.
        *
        * Usamos @BindsInstance para objetos que se construyen fuera del
        * gráfico (por ejemplo, instancias de Context)
        */
        fun create(@BindsInstance context: Context): AppComponent
    }


    fun inject(activity: RegistrationActivity)
    fun inject (activity: MainActivity)


}